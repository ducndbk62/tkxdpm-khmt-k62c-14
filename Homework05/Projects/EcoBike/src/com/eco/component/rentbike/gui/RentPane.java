package com.eco.component.rentbike.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

import com.eco.bean.Vehicle;
import com.eco.component.rentbike.controller.RentingController;

public class RentPane extends JPanel{
	private JTextField barcodeField;
	private JPanel panel;
	private JLabel statusLabel;
	private JLabel nameLabel = new JLabel();
	private JLabel typeLabel = new JLabel();
	private JLabel weightLabel = new JLabel();
	private JLabel costLabel = new JLabel();
	private JLabel lienceLabel = new JLabel();
	private JLabel manuafLabel = new JLabel();
	private JLabel producerLabel = new JLabel();

	private JButton acceptButton = new JButton("Xác nhận");
	
	private RentingController controller;
    public RentPane(RentingController controller) {
    	this.controller = controller;
    	panel = new JPanel();
    	this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
    	this.setAlignmentX(CENTER_ALIGNMENT);
       JButton home = new JButton("Home");
       
    	add(home);
    	add(Box.createRigidArea(new Dimension(0, 10)));
    	add(new JLabel("Nhập mã xe:"));
    	barcodeField = new JTextField();
    	barcodeField.setMaximumSize(new Dimension(500,30));
    	add(barcodeField);
    	
    	statusLabel = new JLabel();
    	
    	setMaximumSize(new Dimension(400,80));
    	
    	acceptButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!barcodeField.getText().trim().equals("")) {
					Map<String, String> params = new HashMap<String, String>();
					params.put("barcode", barcodeField.getText());
					updateData(controller.searchVehicle(params));
				}
			}
		});
    	add(Box.createRigidArea(new Dimension(0, 10)));
    	add(acceptButton);
    	add(statusLabel);
    	statusLabel = new JLabel();
    	add(statusLabel);
    	setMaximumSize(new Dimension(400,80));
    	
    	home.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				controller.getHomePage();
			}
		});
    }
	protected void updateData(Vehicle vehicle) {
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		this.add(panel);
		panel.removeAll();
		panel.revalidate();
		panel.repaint();
		if(vehicle == null) {
			statusLabel.setText("Check lại mã xem nào");
		}
		else {
			statusLabel.setText("");
			nameLabel.setText("Name : "+vehicle.getName());
	    	panel.add(nameLabel);
	    	typeLabel.setText("Type of Bike : "+vehicle.getTypeBike());
	    	panel.add(typeLabel);
	    	weightLabel.setText("Weight : "+vehicle.getWeight());
	    	panel.add(weightLabel);
	    	costLabel.setText("Cost : "+vehicle.getCost());
	    	panel.add(costLabel);
	    	lienceLabel.setText("license plate : "+vehicle.getLicensePlate());
	    	panel.add(lienceLabel);
	    	manuafLabel.setText("Manuafacturing date : "+vehicle.getManuafacturingDate());
	    	panel.add(manuafLabel);
	    	producerLabel.setText("Producer : "+vehicle.getProducer());
	    	panel.add(producerLabel);
	    	JButton rentButton = new JButton("Thuê Xe");
	    	panel.add(rentButton);
	    	rentButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JPanel frame = new JPanel();
					if(vehicle.getTypeBike().equals("bike")) {
						 JOptionPane.showMessageDialog(frame,
							    "normal-bike : hết 400k",
							    "Nộp tiền cọc nào",
							    JOptionPane.PLAIN_MESSAGE);
					}
					else if(vehicle.getTypeBike().equals("ebike")) {
						JOptionPane.showMessageDialog(frame,
								"E-bike : hết 700k",
							    "Nộp tiền cọc nào",
							    JOptionPane.PLAIN_MESSAGE);
					}else {
						JOptionPane.showMessageDialog(frame,
								"normal-bike : hết 550k",
							    "Nộp tiền cọc nào",
							    JOptionPane.PLAIN_MESSAGE);
					}
					controller.updateStatus(vehicle);
					remove(acceptButton);
					panel.remove(rentButton);
					repaint();
				}
			});
		}
	}
}
