package com.eco.component.pay;

import java.util.Random;

import javax.swing.JPanel;

import com.eco.app.user.EcoUserController;
import com.eco.bean.Vehicle;



public class PayPageController {
	private PayPage payPage;
	private EcoUserController controller;
	public PayPageController(EcoUserController controller) {
		this.controller = controller;
		payPage = new PayPage();
		payPage.setController(this);
		setInformation();
	}
	private void setInformation() {
		payPage.setInformation();
	}
	public JPanel getPayPage() {
		return payPage;
	}
	public int random() {
		Random random = new Random();
		int n = random.nextInt(3);
		return n;
	}
	public int randomTime() {
		Random random = new Random();
		int n = random.nextInt(300);
		return n * 15;
	}
	public int calculateMoney(int type, int time) {
		if (time <= 10)
			return 0;
		int money = (time >= 30)? 10000 + (time - 30) / 15 * 3000 : 10000;
		if (type == 1) {
			return money;
		}
		return money * 3 / 2 ;
	}
	private int loaixe(Vehicle vehicle) {
		if (vehicle.getTypeBike().equals("bike"))
			return 1;
		if (vehicle.getTypeBike().equals("twinbike"))
			return 3;
		return 2;
	}
	public int getType(Vehicle vehicle) {
		return loaixe(vehicle);
	}
	private int coc (Vehicle vehicle) {
		if (vehicle.getTypeBike().equals("bike"))
			return 400000;
		if (vehicle.getTypeBike().equals("twinbike"))
			return 550000;
		return 700000;
	}
	public int getCoc(Vehicle vehicle) {
		return coc(vehicle);
	}
	public String calculateMn(Vehicle vehicle, int time) {
		int tien = calculateMoney(loaixe(vehicle), time);
		int coc = coc(vehicle);
		return (coc > tien) ? " \n Số tiền hoàn trả: " + (coc - tien) + "đ": " \n Số tiền cần trả thêm: " + (tien - coc) + "đ"; 
	}
	public Vehicle getRentingVehicle() {
		return controller.getRentingVehicle();
	}
	public void getHomePage() {
		controller.getHomePage();
	}
	public void deleteRentingVehicle() {
		controller.deleteRentingVehicle();
	}
}