package com.eco.component.pay;

import javax.swing.JPanel;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.eco.app.user.EcoUserController;
import com.eco.bean.Vehicle;
import com.eco.component.rentbike.controller.RentingController;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PayPage extends JPanel {
 
	private JTextPane textPane ;
	private JLabel lblNewLabel ;
	private PayPageController payPageController;
	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 */
	public PayPage() {
		
		lblNewLabel =  new JLabel("Thanh to\u00E1n");
		textPane = new JTextPane();
		JButton btnNewButton = new JButton("X\u00E1c nh\u1EADn");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				payPageController.deleteRentingVehicle();
		
				JOptionPane.showMessageDialog(null, "Thanh toán thành công", "Thông báo", JOptionPane.YES_OPTION);
				setVisible(false);
				payPageController.getHomePage();
			}
		});
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(59)
							.addComponent(textPane, GroupLayout.PREFERRED_SIZE, 308, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(186)
							.addComponent(lblNewLabel)))
					.addContainerGap(83, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(307, Short.MAX_VALUE)
					.addComponent(btnNewButton)
					.addGap(54))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(31)
					.addComponent(lblNewLabel)
					.addGap(18)
					.addComponent(textPane, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
					.addComponent(btnNewButton)
					.addGap(22))
		);
		setLayout(groupLayout);
	}
	public void setInformation() {
		//vcl
		/*
		EcoUserController controller = new EcoUserController();
		RentingController rentingController = new RentingController(controller);
		*/
		int time = payPageController.randomTime();
		Vehicle bike =payPageController .getRentingVehicle();
		textPane.setText(" \n Tên xe: " + bike.getName() + 
				"\n Loại xe:  " + bike.getTypeBike()+ " \n Thời gian sử dụng: " + time + 
				" phút \n Tiền cọc đã đặt: " + payPageController.getCoc(bike) + 
				"đ \n Tiền thuê xe: " + payPageController.calculateMoney(payPageController.getType(bike), time) + "đ" + 
				payPageController.calculateMn(bike, time));
		
	}
	public void setController (PayPageController payPageController ) {
		this.payPageController = payPageController;
	}
}